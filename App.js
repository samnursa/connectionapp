/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      networkSpeed : 0,
      imageUri : "http://chandra.harvard.edu/graphics/resources/desktops/2006/1e0657_1280.jpg",
      downloadSizeInBits : 1302600
    }
  }

  componentDidMount(){
    this.measureNetworkBandwitdh();
  }

  measureNetworkBandwitdh = () => {
    const {imageUri, downloadSizeInBits} = this.state;
    const startTime = new Date().getTime();
    
    RNFetchBlob
      .config({
          fileCache: false
      })
      .fetch('GET', imageUri, {})
      .then((res)=>{
          const endTime = new Date().getTime();
          const duration = (endTime - startTime)/1000;
          const speed = (downloadSizeInBits/(1024 * 1024 * duration));
          this.setState({
            networkSpeed : speed
          });
      });
  }

  render() {
    const { networkSpeed } = this.state;
    return (
      <View style={styles.container}>
        <Text>Connection Speed : {networkSpeed}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});
